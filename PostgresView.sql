DROP CAST IF EXISTS (TEXT AS INTEGER);
CREATE OR REPLACE FUNCTION CastToInt(TEXT) RETURNS INTEGER AS $$
BEGIN
  RETURN CAST($1::VARCHAR AS INTEGER);
EXCEPTION
  WHEN INVALID_TEXT_REPRESENTATION THEN
    RETURN 0;
END;
$$ LANGUAGE PLPGSQL IMMUTABLE;
CREATE CAST (TEXT AS INTEGER) WITH FUNCTION CastToInt(TEXT);

DROP VIEW IF EXISTS "Products";
CREATE VIEW "Products" AS
SELECT
  COALESCE(product.nr_wewn, 0)                          AS "Id",

  CONCAT(product.nazwa, product.nazwa_)                 AS "Title",

  product.cena_z_n                                      AS "PriceBuyNet",
  product.cena_z_b                                      AS "PriceBuyGross",
  COALESCE(stock.csn, 0)                                AS "PriceSellNet",
  COALESCE(stock.csb, 0)                                AS "PriceSellGross",
  CAST(COALESCE(
    NULLIF(product.miejsce_w_magazynie, ''),
    '0') AS NUMERIC)                                    AS "PriceAdditional",

  CAST(COALESCE(stock.ilosc, 0) AS INTEGER)             AS "Stock",

  CAST(COALESCE(product.vat, '0') AS INTEGER)           AS "Vat",

  COALESCE(product.indeks_znakowy_isbn, '')             AS "Isbn",
  CAST(product.indeks_numeryczny AS TEXT)               AS "Ean",

  LOWER(COALESCE(product.jednostka, 'szt.'))            AS "Unit",

  COALESCE(author.opis, '')                             AS "Author",
  COALESCE(publisher.opis, '')                          AS "Publisher",
  supplier.nazwa                                        AS "Supplier"
FROM
  dane_kat_tow_ AS product
INNER JOIN
  dane_{WAREHOUSE_ID}_stany_mg AS stock
  ON stock.nr_wewn = product.nr_wewn
LEFT JOIN
  dane_pola_ind_autor__21 AS author
  ON author.indeks = product.k1_pochodzenie_autor
LEFT JOIN
  dane_pola_ind_wydawca21 AS publisher
  ON publisher.indeks = product.k2_wydawca
LEFT JOIN
  dane_dostawcy AS supplier
  ON supplier.numer = product.dostawca
ORDER BY "Id";