using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using MagadReports.Backend.Managers;
using MagadReports.Backend.Models.Response;
using Nancy;

namespace MagadReports.Backend.Modules
{
    public class MagadDictionaries : NancyModule
    {
        public MagadDictionaries() : base("/api")
        {
            Get["/suppliers"] = _ =>
            {
                using (var Db = DatabaseManager.Instance.Conn)
                {
                    var suppliers = Db.Query<Supplier>(@"SELECT numer AS Id, nazwa AS Name FROM dane_dostawcy ORDER BY Name");

                    return Response.AsJson(new
                    {
                        suppliers = suppliers
                    });
                }
            };

            Get["/warehouses"] = _ =>
            {
                using (var Db = DatabaseManager.Instance.Conn)
                {
                    var warehouses = Db.Query<Warehouse>(@"SELECT numer AS Id, nazwa AS Name FROM dane_magazyny ORDER BY Name");

                    return Response.AsJson(new
                    {
                        warehouses = warehouses
                    });
                }
            };
        }
    }
}