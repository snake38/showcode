using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using MagadReports.Backend.DatabaseModels;
using MagadReports.Backend.Managers;
using MagadReports.Backend.Models.Response;
using Nancy;

namespace MagadReports.Backend.Modules
{
    public class Reports : NancyModule
    {
        public Reports() : base("/api")
        {
            Get["/reports"] = _ =>
            {
                var reports = new List<Report>();

                reports.Add(new Report
                {
                    Id = 1,
                    Title = "Towary niesprzedane",
                    Description = "Towary, które w ciągu ostatnich N dni nie zostały sprzedane"
                });

                return Response.AsJson(new
                {
                    reports = reports
                });
            };

            Get["/reports/created"] = _ =>
            {
                IEnumerable<MagadReport> magadReports;

                using (var Db = DatabaseManager.Instance.Conn)
                {
                    magadReports = Db.Query<MagadReport>(@"SELECT * FROM MagadReports ORDER BY CreatedAt DESC");
                }

                var reportsResponse = new List<object>();

                foreach (var report in magadReports)
                {
                    reportsResponse.Add(new
                    {
                        hash = report.Hash,
                        reportName = report.ReportName,
                        date = report.CreatedAt.ToString("dd/MM/yyyy HH:mm"),
                        status = report.Status
                    });
                }

                return Response.AsJson(new
                {
                    reports = reportsResponse
                });
            };

            Get["/report/{hash}/status"] = _ =>
            {
                MagadReport report = null;
                using (var Db = DatabaseManager.Instance.Conn)
                {
                    report = Db.Query<MagadReport>(@"SELECT * FROM MagadReports WHERE Hash = @Hash", new
                    {
                        Hash = _.hash
                    }).FirstOrDefault();
                }

                if (report == null)
                {
                    return Response.AsJson(new
                    {
                        status = "not found"
                    }, HttpStatusCode.BadRequest);
                }

                return Response.AsJson(new
                {
                    reportName = report.ReportName,
                    progress = report.Progress,
                    date = report.CreatedAt.ToString("dd/MM/yyyy HH:mm"),
                    status = report.Status
                });
            };
        }
    }
}