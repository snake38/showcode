import React from "react";

import $ from "jquery";
import Ajax from "../../ajax.js";

import CheckboxList from "../CheckboxList/CheckboxList.js";
import DateInput from "../DateInput/DateInput.js";
import IntegerInput from "../IntegerInput/IntegerInput.js";

export default
class NewReportForm extends React.Component {
	static contextTypes = {
		flux: React.PropTypes.object.isRequired,
		router: React.PropTypes.any.isRequired
	}

	constructor(props) {
		super(props);

		this.state = {
			inputs: [],
			generating: false,
			warehouses: [],
			suppliers: []
		}
	}

	componentWillMount() {
		this.NewReportFormActions = this.context.flux.getActions("NewReportFormActions");
		this.NewReportFormStore = this.context.flux.getStore("NewReportFormStore");
	}

	componentDidMount() {
		this.NewReportFormStore.addListener("change", this.getFromStore);
		this.NewReportFormActions.getReportInputs(this.props.params.id);
		this.NewReportFormActions.getWarehouses();
		this.NewReportFormActions.getSuppliers();
	}

	componentWillUnmount() {
		this.NewReportFormStore.removeListener("change", this.getFromStore);
	}

	getFromStore = () => {
		this.setState({
			inputs: this.NewReportFormStore.state.inputs,
			warehouses: this.NewReportFormStore.state.warehouses,
			suppliers: this.NewReportFormStore.state.suppliers,
		});
	}

	render() {
		var inputs = this.state.inputs.map((input, i) => {
			var inputHtml = null;
			if (input.type == "text") {
				inputHtml = <input text={input.type} name={input.name} />;
			}
			else if (input.type == "integer") {
				inputHtml = <IntegerInput name={input.name} />;
			}
			else if (input.type == "date") {
				inputHtml = <DateInput name={input.name} />;
			}
			else if (input.type == "warehouses") {
				inputHtml = <CheckboxList 
					items={this.state.warehouses} 
					name={input.name} />
			}
			else if (input.type == "suppliers") {
				inputHtml = <CheckboxList 
					items={this.state.suppliers} 
					name={input.name} />
			}
			else {
				inputHtml = <b>Nieprawidłowy typ pola wpisywania.</b>;
			}

			return (
				<tr key={i}>
					<td className="uk-width-1-2 uk-text-center">
						<label className="uk-form-label" htmlFor={input.name}>
							{input.displayName}<br />
						</label>
						<div className="uk-form-controls">
							{inputHtml}
						</div>
					</td>
					<td className="uk-width-1-2">
						{input.description}
					</td>
				</tr>
			);
		});

		var submitButton = null;
		if (!this.state.generating) {
			submitButton = <button 
				className="uk-button uk-button-primary uk-button-large">
				Generuj</button>;
		}
		else {
			submitButton = <button disabled
				className="uk-button uk-button-primary uk-button-large">
				Generuje...</button>;
		}

		return (
			<form onSubmit={this.onSubmit} id="inputData" className="uk-form uk-form-stacked">
				<table className="uk-table uk-table-hover uk-table-striped">
					<caption>Dane wejściowe</caption>
					<tbody>
						{inputs}

						<tr className="uk-text-center">
							<td colSpan="2">
								{submitButton}
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		);
	}

	onSubmit = e => {
		e.preventDefault();

		this.setState({
			generating: true
		});

		var json = {
			inputs: []
		};

		var formData = $("#inputData").serializeArray();
		formData.forEach(item => {
			json.inputs.push(item);
		});

		Ajax.POST("api/report/" + this.props.params.id + "/generate", json,
			data => {
				this.context.router.transitionTo("report", {
					hash: data.hash
				});
			},
			data => {
				if (data.status == 400) {
					UIkit.notify({
						message: `<i class="uk-icon-exclamation"></i> ` + 
							data.responseJSON.message,
						status: "warning",
						timeout: 5000
					});
				}
				else {
					console.log(data);
					UIkit.notify({
						message: `<i class="uk-icon-exclamation"></i> Nieznany błąd`,
						status: "warning",
						timeout: 5000
					});
				}

				this.setState({
					generating: false
				});
			});
	}
};